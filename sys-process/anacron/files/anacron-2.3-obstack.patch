--- readtab.c.orig	Sat Jul 22 15:14:53 2006
+++ readtab.c	Sat Jul 22 15:27:14 2006
@@ -19,6 +19,14 @@
  
     The GNU General Public License can also be found in the file
     `COPYING' that comes with the Anacron source distribution.
+
+    Changes:
+
+    May 2003 (Derik van Zuetphen)
+	replaced obstack with malloc/realloc calls
+
+    Jul 2006 (Timothy Redaelli)
+        conditional patch to replace obstack with malloc/realloc calls
 */
 
 
@@ -29,16 +37,25 @@
 #include <errno.h>
 #include <stdio.h>
 #include <stdlib.h>
-#include <obstack.h>
 #include <limits.h>
 #include <fnmatch.h>
 #include <unistd.h>
 #include <signal.h>
 #include "global.h"
 #include "matchrx.h"
+#ifdef __linux__
+#include <obstack.h>
+
+static struct obstack input_o;
+static struct obstack tab_o;
+
+/* some definitions for the obstack macros */
+#define obstack_chunk_alloc xmalloc
+#define obstack_chunk_free free
+#else
+#define MAXTABLINE	1000
+#endif
 
-static struct obstack input_o;   /* holds input line */
-static struct obstack tab_o;    /* holds processed data read from anacrontab */
 static FILE *tab;
 job_rec **job_array;
 int njobs;                       /* number of jobs to run */
@@ -47,10 +64,6 @@
 static job_rec *last_job_rec;    /* last job stored in memory, at the moment */
 static env_rec *last_env_rec;    /* last environment assignment stored */
 
-/* some definitions for the obstack macros */
-#define obstack_chunk_alloc xmalloc
-#define obstack_chunk_free free
-
 static void *
 xmalloc (size_t size)
 /* Just like standard malloc(), only never returns NULL. */
@@ -63,6 +76,20 @@
     return ptr;
 }
 
+#ifndef __linux__
+static void *
+xrealloc (void *mem, size_t size)
+/* Just like standard realloc(), only never returns NULL. */
+{
+    void * ptr;
+
+    ptr = realloc(mem,size);
+    if (ptr == NULL)
+	die("Memory exhausted");
+    return ptr;
+}
+#endif
+
 static int
 conv2int(const char *s)
 /* Return the int or -1 on over/under-flow
@@ -78,21 +105,36 @@
 }
 
 static char *
-read_tab_line ()
+#ifndef __linux__
+read_tab_line (char *line)
 /* Read one line and return a pointer to it.
 Return NULL if no more lines.
  */
 {
     int c;
+    int i = 0;
 
     if (feof(tab)) return NULL;
+    while (i < MAXTABLINE-1 && (c = getc(tab)) != EOF && c != '\n')
+	line[i++] = c;
+    if (ferror(tab)) die_e("Error reading %s", anacrontab);
+    line[i] = 0;
+    return line;
+#else
+read_tab_line ()
+{
+    int c;
+    
+    if (feof(tab)) return NULL;
     while ((c = getc(tab)) != EOF && c != '\n')
-	obstack_1grow(&input_o, c);
+        obstack_1grow(&input_o, c);
     if (ferror(tab)) die_e("Error reading %s", anacrontab);
     obstack_1grow(&input_o, '\0');
     return obstack_finish(&input_o);
+#endif
 }
 
+
 static int
 job_arg_num(const char *ident)
 /* Return the command-line-argument number refering to this job-identifier.
@@ -119,8 +161,13 @@
 
     var_len = strlen(env_var);
     val_len = strlen(value);
+#ifndef __linux__
+    er = (env_rec*)xmalloc(sizeof(env_rec));
+    er->assign = (char*)xmalloc(var_len + 1 + val_len + 1);
+#else
     er = obstack_alloc(&tab_o, sizeof(env_rec));
     er->assign = obstack_alloc(&tab_o, var_len + 1 + val_len + 1);
+#endif
     strcpy(er->assign, env_var);
     er->assign[var_len] = '=';
     strcpy(er->assign + var_len + 1, value);
@@ -151,14 +198,26 @@
 		 anacrontab, line_num);
 	return;
     }
+#ifndef __linux__
+    jr = (job_rec*)xmalloc(sizeof(job_rec));
+#else
     jr = obstack_alloc(&tab_o, sizeof(job_rec));
+#endif
     jr->period = period;
     jr->delay = delay;
     jr->tab_line = line_num;
-    jr->ident = obstack_alloc(&tab_o, ident_len + 1);
+#ifndef __linux__
+    jr->ident = (char*)xmalloc(ident_len + 1);
+#else
+    jr = obstack_alloc(&tab_o, sizeof(job_rec));
+#endif
     strcpy(jr->ident, ident);
     jr->arg_num = job_arg_num(ident);
+#ifndef __linux__
+    jr->command = (char*)xmalloc(command_len + 1);
+#else
     jr->command = obstack_alloc(&tab_o, command_len + 1);
+#endif
     strcpy(jr->command, command);
     jr->job_pid = jr->mailer_pid = 0;
     if (last_job_rec != NULL) last_job_rec->next = jr;
@@ -222,8 +281,11 @@
 read_tab()
 /* Read the anacrontab file into memory */
 {
+#ifndef __linux__
+    char tab_line[MAXTABLINE];
+#else
     char *tab_line;
-
+#endif
     first_job_rec = last_job_rec = NULL;
     first_env_rec = last_env_rec = NULL;
     jobs_read = 0;
@@ -231,6 +293,12 @@
     /* Open the anacrontab file */
     tab = fopen(anacrontab, "r");
     if (tab == NULL) die_e("Error opening %s", anacrontab);
+#ifndef __linux__
+    while ((read_tab_line(tab_line)) != NULL)
+    {
+	line_num++;
+	parse_tab_line(tab_line);
+#else
     /* Initialize the obstacks */
     obstack_init(&input_o);
     obstack_init(&tab_o);
@@ -239,7 +307,8 @@
 	line_num++;
 	parse_tab_line(tab_line);
 	obstack_free(&input_o, tab_line);
-    }
+#endif
+}
     if (fclose(tab)) die_e("Error closing %s", anacrontab);
 }
 
@@ -269,17 +338,27 @@
 
     j = first_job_rec;
     njobs = 0;
+#ifndef __linux__
+    job_array = NULL;
+#endif
     while (j != NULL)
     {
 	if (j->arg_num != -1 && (update_only || consider_job(j)))
 	{
+#ifndef __linux__
+	    job_array = (job_rec**)xrealloc(job_array, (njobs+1)*sizeof(j));
+	    job_array[njobs] = j;
+	    njobs++;
+#else
 	    njobs++;
 	    obstack_grow(&tab_o, &j, sizeof(j));
-	}
+#endif
 	j = j->next;
     }
-    job_array = obstack_finish(&tab_o);
-
+#ifdef __linux__
+	job_array = obstack_finish(&tab_o);
+#endif
+    }
     /* sort the jobs */
     qsort(job_array, njobs, sizeof(*job_array),
 	  (int (*)(const void *, const void *))execution_order);
