# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils games

MY_P="${PN}-patch-${PV}.tar.gz"
DESCRIPTION="Uplink is a commercial hacking strategy game."
HOMEPAGE="http://www.uplink.co.uk/"
SRC_URI="http://www.uplink.co.uk/test/${MY_P}"

LICENSE="uplink"
SLOT="0"
KEYWORDS="-* ~amd64 ~x86"
RESTRICT="strip mirror"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}
	app-arch/unzip"

dir=${GAMES_PREFIX_OPT}/${PN}
Ddir=${D}/${dir}

upl_versions=(
	"1.0"
	"1.31"
)
upl_files=(
	"uplink.zip"
	"uplink-complete-1.31.sh"
)

uplink_find_file() {
	local searchdir=${1}
	local i=0

	for f in ${upl_files[*]}
	do
		if [ -e "${searchdir}/${f}" ] ; then
			export upl_file=${f}
			export upl_version=${upl_versions[${i}]}
		else
			i=$((${i}+1))
		fi
	done
}

pkg_setup() {
	export upl_version="0"
	export upl_file=""

	games_pkg_setup

	# If the file still exists from an earlier install, assume its the same
	# Otherwise copy it from the CD
	uplink_find_file "${PORTAGE_ACTUAL_DISTDIR}"

	# No local copy - try the CD drive
	if [[ "${upl_version}" == "0" ]] ; then
		cdrom_get_cds linux
		local searchdir="${CDROM_ROOT}/linux"
		uplink_find_file "${searchdir}"

		if [[ "${upl_version}" != "0" ]] ; then
			einfo "Found the original Uplink CD (${upl_version})"
			einfo "Copying ${upl_file} to ${PORTAGE_ACTUAL_DISTDIR}"
			cp "${searchdir}/${upl_file}" "${PORTAGE_ACTUAL_DISTDIR}/${upl_file}"
		fi

	fi

	if [[ "${upl_version}" == "0" ]] ; then
		eerror "Unable to find a known Uplink CD ROM"
		eerror "If you believe you have a valid Uplink CD ROM, please report this bug attaching"
		eerror " details of the name and location of the linux installer on the CD ROM"
		die
	fi
}

src_install() {
	dodir "${dir}"
	cd "${Ddir}"

	if [[ "${upl_file}" == "uplink.zip" ]] ; then
		# Unzip basic uplink
		unzip -qq "${PORTAGE_ACTUAL_DISTDIR}/${upl_file}"
	else
		# Assume it's a .sh

		# Create a temporary directory to extract the .sh to
		cd "${T}"
		# Extract the .sh using its own routines
		"${PORTAGE_ACTUAL_DISTDIR}/${upl_file}" --noexec --keep --target .
		# Delete unnecessary files and move the executable script to where it expects to be
		rm -rf setup.data setup.sh
		mv bin/Linux/x86/uplink .
		rm -rf bin
		# Copy everything to Ddir
		cp -pPR * "${Ddir}"
		# cd out of T to avoid errors when we delete it.
		cd "${Ddir}"
		# Delete the temporary files
		rm -rf "${T}"
	fi

	if [[ "${upl_version}" != "1.53" ]] ; then
		# Copy over patch files
		cp -pPR "${WORKDIR}"/uplink/* ./
		rm sounds.dat
		mv *.dat lib/
	fi


	# Correct permissions
	chown -R root:games "${Ddir}"
	chmod -R 640 "${Ddir}"/*
	chmod 750 "${Ddir}/uplink"
	if [[ -e "${Ddir}/lib" ]] ; then
		chmod 750 "${Ddir}"/lib
		if [[ -e "${Ddir}"/lib/uplink.bin.x86 ]] ; then
			chmod 750 "${Ddir}"/lib/uplink.bin.x86
		fi
	fi

	games_make_wrapper uplink ./uplink "${dir}" "${dir}"
	make_desktop_entry uplink "Uplink" "" "Game"
}

pkg_postinst() {
	games_pkg_postinst
	if [[ "${upl_version}" != "1.53" ]] ; then
		einfo "The first time you start uplink, it will patch itself and then exit. Restart the game to play."
		einfo "The location of the data files has changed. If you have any custom modifications you will need to move them into the lib/ directory."
	fi
}
