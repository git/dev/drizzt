# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Collection of tools for making Python for S60 programs."
HOMEPAGE="http://www.nbl.fi/~nbl928/ensymble.html"
SRC_URI="http://www.nbl.fi/~nbl928/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-lang/python-2.2"
RDEPEND="${DEPEND}
	dev-libs/openssl"

src_install() {
	dodir "/usr/bin"
	./install.sh "${D}/usr/bin" || die "Cannot install ${PN}"
	dodoc README TODO
	dohtml www/*
	insinto /usr/share/${PN}/examples/
	doins data/*
}
