# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

MY_P=${P/_rb/}

DESCRIPTION="BitTorrent library written in C++ for *nix."
HOMEPAGE="http://www.rasterbar.com/products/libtorrent/"
SRC_URI="mirror://sourceforge/libtorrent/${MY_P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

DEPEND="dev-libs/boost"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${MY_P}

src_compile() {
	local boost_libs
	# If threads were used to build boost, the library files will have a suffix.
	if built_with_use "dev-libs/boost" threads || use built_with_use "dev-libs/boost" threads-only ; then
		boost_libs="--with-boost-date-time=mt --with-boost-filesystem=mt --with-boost-thread=mt --with-boost-regex=mt --with-boost-program_options=mt"
	else
		die "libtorrent_rb needs dev-libs/boost built with threads USE flag"
	fi

	econf $(use_enable debug) ${boost_libs} || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS NEWS README
}
