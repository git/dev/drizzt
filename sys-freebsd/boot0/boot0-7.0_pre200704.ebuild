# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-freebsd/boot0/boot0-6.2.ebuild,v 1.1 2007/01/15 21:33:57 drizzt Exp $

inherit bsdmk freebsd

DESCRIPTION="FreeBSD's bootloader"
SLOT="0"
KEYWORDS=" ~x86-fbsd"

IUSE=""

SRC_URI="http://84.33.1.46/~drizzt/stages/${SYS}.tar.bz2"

RDEPEND=""
DEPEND="=sys-freebsd/freebsd-mk-defs-${RV}*
	=sys-freebsd/freebsd-lib-${RV}*"

S="${WORKDIR}/sys/boot"

PATCHES="${FILESDIR}/boot0-7.0-gentoo.patch
	${FILESDIR}/freebsd-sources-6.2-sparc64.patch"

src_unpack() {
	freebsd_src_unpack

	grep -lr --null -- -ffreestanding "${S}" | xargs -0 sed -i -e \
		"s:-ffreestanding:-ffreestanding $(test-flags -fno-stack-protector -fno-stack-protector-all):g" || die
#	sed -i -e '/-fomit-frame-pointer/d' "${S}"/i386/boot2/Makefile || die
	sed -i -e 's/^BOOT2_UFS?\=.*/BOOT2_UFS?=UFS2_ONLY/'	"${S}"/i386/boot2/Makefile || die
}

src_install() {
	dodir /boot/defaults

	mkinstall FILESDIR=/boot || die "mkinstall failed"
}
