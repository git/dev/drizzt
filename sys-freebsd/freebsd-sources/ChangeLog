# ChangeLog for sys-freebsd/freebsd-sources
# Copyright 1999-2007 Gentoo Foundation; Distributed under the GPL v2
# $Header: /var/cvsroot/gentoo-x86/sys-freebsd/freebsd-sources/ChangeLog,v 1.35 2007/01/15 21:40:03 drizzt Exp $

*freebsd-sources-6.2 (15 Jan 2007)

  15 Jan 2007; Timothy Redaelli <drizzt@gentoo.org>
  -freebsd-sources-6.2_rc2.ebuild, +freebsd-sources-6.2.ebuild:
  Bump to 6.2 and remove old version.

  05 Jan 2007; Diego Pettenò <flameeyes@gentoo.org>
  -files/freebsd-sources-6.1-gcc41.patch, -files/SA-06-16-smbfs.patch,
  -files/freebsd-sources-6.1-devfs-deadlock.patch,
  -files/freebsd-sources-6.1-intrcnt.patch, -freebsd-sources-6.1-r3.ebuild,
  -freebsd-sources-6.1-r4.ebuild:
  Remove 6.1 ebuild.

*freebsd-sources-6.2_rc2 (29 Dec 2006)

  29 Dec 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.2_beta3.ebuild, -freebsd-sources-6.2_rc1.ebuild,
  +freebsd-sources-6.2_rc2.ebuild:
  Bump to rc2 and remove older versions.

*freebsd-sources-6.2_rc1 (19 Nov 2006)

  19 Nov 2006; Diego Pettenò <flameeyes@gentoo.org>
  +freebsd-sources-6.2_rc1.ebuild:
  Version bump to 6.2-RC1.

  18 Nov 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.2_beta2.ebuild:
  Remove 6.2_beta2.

*freebsd-sources-6.2_beta3 (01 Nov 2006)

  01 Nov 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-6.2-gcc41.patch, +freebsd-sources-6.2_beta3.ebuild:
  Bump to 6.2_beta3.

  23 Oct 2006; Javier Villavicencio <the_paya@gentoo.org>
  freebsd-sources-6.2_beta2.ebuild:
  Closing bug #151626.

  21 Oct 2006; Diego Pettenò <flameeyes@gentoo.org>
  files/digest-freebsd-sources-6.2_beta2, Manifest:
  Fix digest.

  19 Oct 2006; Javier Villavicencio <the_paya@gentoo.org>
  +files/freebsd-sources-6.1-ntfs.patch, freebsd-sources-6.1-r4.ebuild:
  Fixes bug #151626, thanks to Simon Koenig for reporting and testing.

  19 Oct 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  Remove 6.2_beta1 ebuild.

  19 Oct 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.2_beta1.ebuild:
  Remove 6.2_beta1 ebuild.

  17 Oct 2006; Roy Marples <uberlord@gentoo.org>
  +files/freebsd-sources-6.2-sparc64.patch,
  freebsd-sources-6.2_beta2.ebuild:
  Added ~sparc-fbsd keyword.

*freebsd-sources-6.2_beta2 (05 Oct 2006)

  05 Oct 2006; Diego Pettenò <flameeyes@gentoo.org>
  +freebsd-sources-6.2_beta2.ebuild:
  Version 6.2_beta2.

  21 Sep 2006; Diego Pettenò <flameeyes@gentoo.org>
  freebsd-sources-6.2_beta1.ebuild:
  This time is the right one.

  21 Sep 2006; Diego Pettenò <flameeyes@gentoo.org>
  freebsd-sources-6.2_beta1.ebuild:
  Actually, apply again the devfs patch.

*freebsd-sources-6.2_beta1 (21 Sep 2006)

  21 Sep 2006; Diego Pettenò <flameeyes@gentoo.org>
  +freebsd-sources-6.2_beta1.ebuild:
  Add 6.2_beta1 ebuilds.

*freebsd-sources-6.1-r4 (04 Sep 2006)

  04 Sep 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-6.1-devfs-deadlock.patch,
  +freebsd-sources-6.1-r4.ebuild:
  Add patch from Alex for sandbox not to deadlock devfs, see bug #146284.

  27 Jul 2006; Diego Pettenò <flameeyes@gentoo.org>
  -files/SA-06-04-ipfw.patch, -files/SA-06-06-kmem60.patch,
  -files/SA-06-11-ipsec.patch, -files/freebsd-sources-6.0-gentoover.patch,
  -files/SA-06-05-80211.patch, -files/SA-06-07-pf.patch,
  -files/SA-06-14-fpu.patch, -freebsd-sources-6.1-r1.ebuild,
  -freebsd-sources-6.1-r2.ebuild:
  Drop old versions and stray patches.

  27 Jul 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.0-r5.ebuild:
  Remove 6.0 version.

*freebsd-sources-6.1-r3 (08 Jul 2006)

  08 Jul 2006; Javier Villavicencio <the_paya@gentoo.org>
  +files/freebsd-sources-6.1-intrcnt.patch, freebsd-sources-6.1-r1.ebuild,
  freebsd-sources-6.1-r2.ebuild, +freebsd-sources-6.1-r3.ebuild:
  New patch for a bug noticed with new binutils, fixes bug #139431. Also fixes
  symlink creation.

*freebsd-sources-6.1-r2 (12 Jun 2006)

  12 Jun 2006; Diego Pettenò <flameeyes@gentoo.org>
  files/freebsd-sources-6.1-gcc41.patch, freebsd-sources-6.1-r1.ebuild,
  +freebsd-sources-6.1-r2.ebuild:
  Add new patch for GCC 4.1 from Javier.

*freebsd-sources-6.1-r1 (01 Jun 2006)
*freebsd-sources-6.0-r5 (01 Jun 2006)

  01 Jun 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-06-16-smbfs.patch, -freebsd-sources-6.0-r4.ebuild,
  +freebsd-sources-6.0-r5.ebuild, -freebsd-sources-6.1.ebuild,
  +freebsd-sources-6.1-r1.ebuild:
  Revision bumps to fix security issue SA-06:16.

  31 May 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-6.1-gcc41.patch, freebsd-sources-6.1.ebuild:
  Add patch to fix bug #134870 (building with GCC 4.1.

  24 May 2006; Diego Pettenò <flameeyes@gentoo.org>
  freebsd-sources-6.1.ebuild:
  Add binchecks restrict for newer portage, and fix quoting.

*freebsd-sources-6.1 (09 May 2006)

  09 May 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.1_rc2.ebuild, +freebsd-sources-6.1.ebuild:
  Update to 6.1-RELEASE.

*freebsd-sources-6.1_rc2 (02 May 2006)

  02 May 2006; Diego Pettenò <flameeyes@gentoo.org>
  -freebsd-sources-6.1_rc1.ebuild, +freebsd-sources-6.1_rc2.ebuild:
  Update to 6.1_rc2.

  01 May 2006; Diego Pettenò <flameeyes@gentoo.org>
  freebsd-sources-6.0-r4.ebuild, freebsd-sources-6.1_rc1.ebuild:
  Disable SSP for kernel, of course.

*freebsd-sources-6.1_rc1 (30 Apr 2006)

  30 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +freebsd-sources-6.1_rc1.ebuild:
  Update to 6.1, replace gentoover patch with a simple script.

*freebsd-sources-6.0-r4 (19 Apr 2006)

  19 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-06-14-fpu.patch, -freebsd-sources-6.0-r3.ebuild,
  +freebsd-sources-6.0-r4.ebuild:
  Bump with patch for SA-06:14 patch.

*freebsd-sources-6.0-r3 (12 Apr 2006)

  12 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-6.0-werror.patch,
  files/freebsd-sources-gentoo.patch, -freebsd-sources-6.0-r2.ebuild,
  +freebsd-sources-6.0-r3.ebuild:
  Add patch to drop -Werror and depend on a fixed version of freebsd-mk-defs
  so that a simple 'make' works.

  04 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-6.0-asm.patch, freebsd-sources-6.0-r2.ebuild:
  Add patch to fix building with binutils 2.16.91.0.x.

*freebsd-sources-6.0-r2 (03 Apr 2006)

  03 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-06-11-ipsec.patch, +files/freebsd-sources-gentoo.patch,
  -freebsd-sources-6.0-r1.ebuild, +freebsd-sources-6.0-r2.ebuild:
  Version bump adding patch for SA-06:11.

  02 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  -files/SA-05-09-htt5.patch, -files/SA-05-13-ipfw.patch,
  -files/SA-05-15-tcp.patch, -files/SA-05-17-devfs.patch,
  -files/SA-05-19-ipsec.patch, -files/freebsd-sources-gentoo.patch:
  Drop obsolete files.

*freebsd-sources-6.0-r1 (01 Apr 2006)

  01 Apr 2006; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-05-09-htt5.patch, +files/SA-05-13-ipfw.patch,
  +files/SA-05-15-tcp.patch, +files/SA-05-17-devfs.patch,
  +files/SA-05-19-ipsec.patch, +files/SA-06-05-80211.patch,
  +files/freebsd-sources-6.0-gentoover.patch, +files/SA-06-04-ipfw.patch,
  +files/SA-06-06-kmem60.patch, +files/SA-06-07-pf.patch,
  +files/freebsd-sources-6.0-flex-2.5.31.patch,
  +files/freebsd-sources-gentoo.patch, +metadata.xml,
  +freebsd-sources-6.0-r1.ebuild:
  Import into portage.

  23 Feb 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  Apply all the security patches missing.

  23 Feb 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  Make sure that we don't end up having sys-${RV} being a directory.

  21 Feb 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  ADd patch to build kernel with newer flex.

  13 Feb 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  Do the ${RV} symlink only if the ${PVR} is different.

  13 Feb 2006; Diego Pettenò <flameeyes@gentoo.org> ChangeLog:
  Make sure a sys-${RV} symlink is present, so that we can have a sys
  directory for the different releases (useful while building stuff.

*freebsd-sources-5.4-r4 (25 Aug 2005)

  25 Aug 2005; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-05-09-htt5.patch, +files/SA-05-13-ipfw.patch,
  +files/SA-05-15-tcp.patch, +files/SA-05-17-devfs.patch,
  +files/SA-05-19-ipsec.patch, +files/freebsd-sources-gentoo.patch,
  +freebsd-sources-5.4-r4.ebuild:
  Moved to sys-freebsd.

*freebsd-sources-5.4-r3 (21 Jul 2005)

  21 Jul 2005; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-05-17-devfs.patch, files/freebsd-sources-gentoo.patch,
  -files/freebsd-sources-gentoo-gcc.patch, -freebsd-sources-5.4-r2.ebuild,
  +freebsd-sources-5.4-r3.ebuild:
  Update to patch for FreeBSD-SA-05:17.devfs and added branding.

  15 Jul 2005; Diego Pettenò <flameeyes@gentoo.org>
  freebsd-sources-5.4-r2.ebuild:
  Added symlink useflag, fixed symlink creation for -rX ebuilds. Added nostrip
  restriction (it's a sources ebuild).

*freebsd-sources-5.4-r2 (30 Jun 2005)
*freebsd-sources-5.3-r2 (30 Jun 2005)

  30 Jun 2005; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-05-13-ipfw.patch, +files/SA-05-15-tcp.patch,
  +freebsd-sources-5.3-r2.ebuild, +freebsd-sources-5.4-r2.ebuild:
  Added patches for Security Advisories 05:13-ipfw and 05:15-tcp.

  13 May 2005; Diego Pettenò <flameeyes@gentoo.org>
  +files/freebsd-sources-gentoo.patch, freebsd-sources-5.3-r1.ebuild,
  freebsd-sources-5.4-r1.ebuild:
  Slotted, now installing in /usr/src/sys-${PVR} similarly to linux.

*freebsd-sources-5.4-r1 (13 May 2005)
*freebsd-sources-5.3-r1 (13 May 2005)

  13 May 2005; Diego Pettenò <flameeyes@gentoo.org>
  +files/SA-05-09-htt5.patch, -files/ich-sound.patch,
  -freebsd-sources-5.3.ebuild, +freebsd-sources-5.3-r1.ebuild,
  -freebsd-sources-5.4.ebuild, +freebsd-sources-5.4-r1.ebuild:
  Added security patch for advisory FreeBSD-SA-05:09.htt.

*freebsd-sources-5.4_rc4 (05 May 2005)

  05 May 2005; Diego Pettenò <flameeyes@gentoo.org>
  +freebsd-sources-5.4_rc4.ebuild:
  Added 5.4_rc4.

  15 Sep 2004; Otavio Piske <angusyoung@gentoo.org> freebsd-sources-5.2.1.ebuild:
  Initial release

*freebsd-sources-5.2.1.ebuild (15 Sep 2004)
