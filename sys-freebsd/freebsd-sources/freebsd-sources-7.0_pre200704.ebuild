# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-freebsd/freebsd-sources/freebsd-sources-6.2.ebuild,v 1.1 2007/01/15 21:40:03 drizzt Exp $

inherit bsdmk freebsd flag-o-matic

DESCRIPTION="FreeBSD kernel sources"
SLOT="${PVR}"
KEYWORDS=" ~x86-fbsd"

IUSE="symlink"

SRC_URI="http://84.33.1.46/~drizzt/stages/${SYS}.tar.bz2"

RDEPEND=">=sys-freebsd/freebsd-mk-defs-7.0_pre"
DEPEND=""

RESTRICT="strip binchecks"

S="${WORKDIR}/sys"

MY_PVR="${PVR}"

[[ ${MY_PVR} == "${RV}" ]] && MY_PVR="${MY_PVR}-r0"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# This replaces the gentoover patch, it doesn't need reapply every time.
	sed -i -e 's:^REVISION=.*:REVISION="'${PVR}'":' \
		-e 's:^BRANCH=.*:BRANCH="Gentoo":' \
		-e 's:^VERSION=.*:VERSION="${TYPE} ${BRANCH} ${REVISION}":' \
		"${S}/conf/newvers.sh"

	#epatch "${FILESDIR}/${PN}-gentoo.patch"
	epatch "${FILESDIR}/${PN}-6.0-flex-2.5.31.patch"
	epatch "${FILESDIR}/${PN}-6.0-asm.patch"
	#epatch "${FILESDIR}/${PN}-6.0-werror.patch"
	#epatch "${FILESDIR}/${PN}-6.2-gcc41.patch"
	#epatch "${FILESDIR}/${PN}-6.2-sparc64.patch"
	epatch "${FILESDIR}/${PN}-6.1-ntfs.patch"

	# Remove -Werror
	sed -i -e 's/-Werror//' "${S}"/conf/{kmod,kern.pre}.mk

	# Disable SSP for the kernel
	grep -Zlr -- -ffreestanding "${S}" | xargs -0 sed -i -e \
		"s:-ffreestanding:-ffreestanding $(test-flags -fno-stack-protector -fno-stack-protector-all):g"

	# Filter some FreeBSD-gcc only CFLAGS
	fgrep -Zlr -- -mno-align-long-strings "${S}" | xargs -0 sed -i -e \
	's/-mno-align-long-strings//g'
	sed -i -e 's/-fformat-extensions//' "${S}"/conf/kern.mk

	# Use -lfl instead of -ll for flex
	sed -i -e 's/LDADD=	-ll/LDADD=	-lfl/' "${S}"/dev/aic7xxx/aicasm/Makefile

	# Remove the need of __FreeBSD_cc_version
	sed -i -e 's/__FreeBSD_cc_version >= 300001 &&//' "${S}"/sys/sys/cdefs.h

	# FIXME GCC 4.1 fixes
	sed -i -e '/^static devclass_t ucom_devclass;$/d' "${S}"/dev/usb/ufoma.c
}

src_compile() {
	:
}

src_install() {
	insinto "/usr/src/sys-${MY_PVR}"
	doins -r "${S}/"*
}

pkg_postinst() {
	if [[ ! -L "${ROOT}/usr/src/sys" ]]; then
		einfo "/usr/src/sys symlink doesn't exist; creating symlink to sys-${MY_PVR}..."
		ln -sf "sys-${MY_PVR}" "${ROOT}/usr/src/sys" || \
			eerror "Couldn't create ${ROOT}/usr/src/sys symlink."
		# just in case...
		[[ -L ""${ROOT}/usr/src/sys-${RV}"" ]] && rm "${ROOT}/usr/src/sys-${RV}"
		ln -sf "sys-${MY_PVR}" "${ROOT}/usr/src/sys-${RV}" || \
			eerror "Couldn't create ${ROOT}/usr/src/sys-${RV} symlink."
	elif use symlink; then
		einfo "Updating /usr/src/sys symlink to sys-${MY_PVR}..."
		rm "${ROOT}/usr/src/sys" "${ROOT}/usr/src/sys-${RV}" || \
			eerror "Couldn't remove previous symlinks, please fix manually."
		ln -sf "sys-${MY_PVR}" "${ROOT}/usr/src/sys" || \
			eerror "Couldn't create ${ROOT}/usr/src/sys symlink."
		ln -sf "sys-${MY_PVR}" "${ROOT}/usr/src/sys-${RV}" || \
			eerror "Couldn't create ${ROOT}/usr/src/sys-${RV} symlink."
	fi

	if use sparc-fbsd ; then
		ewarn "WARNING: kldload currently causes kernel panics"
		ewarn "on sparc64. This is probably a gcc-4.1 issue, but"
		ewarn "we need gcc-4.1 to compile the kernel correctly :/"
		ewarn "Please compile all modules you need into the kernel"
	fi
}
