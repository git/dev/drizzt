# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-freebsd/freebsd-share/freebsd-share-6.2.ebuild,v 1.2 2007/01/18 21:14:00 drizzt Exp $

inherit bsdmk freebsd

DESCRIPTION="FreeBSD shared tools/files"
SLOT="0"
KEYWORDS="~sparc-fbsd ~x86-fbsd"

IUSE="doc isdn"

SRC_URI="http://84.33.1.46/~drizzt/stages/${SHARE}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${CONTRIB}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${GNU}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${UBIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${USBIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${SBIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${BIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${LIB}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${ETC}.tar.bz2"

DEPEND="=sys-freebsd/freebsd-mk-defs-${RV}*"
RDEPEND=""

RESTRICT="strip"

S="${WORKDIR}/share"

pkg_setup() {
	use isdn || mymakeopts="${mymakeopts} NO_I4B= "
	use doc || mymakeopts="${mymakeopts} NO_SHAREDOCS= "

	mymakeopts="${mymakeopts} NO_SENDMAIL= NO_MANCOMPRESS= NO_INFOCOMPRESS= NO_LOCALES="
}

REMOVE_SUBDIRS="mk termcap zoneinfo tabset"

PATCHES="${FILESDIR}/${PN}-5.3-doc-locations.patch
	${FILESDIR}/${PN}-5.4-gentoo-skel.patch"
#	${FILESDIR}/${PN}-7.0-locale.patch"

src_unpack() {
	freebsd_src_unpack

	# Remove make.conf manpage as it describes bsdmk's make.conf.
	sed -i -e 's:make.conf.5::' "${S}/man/man5/Makefile"
	# Remove mailer.conf manpage
	sed -i -e 's:mailer.conf.5::' "${S}/man/man5/Makefile"
	# Remove pbm manpage
	sed -i -e 's:pbm.5::' "${S}/man/man5/Makefile"
	# Remove security manpage
	sed -i -e 's:security.7::' "${S}/man/man7/Makefile"
	# Remove builtins manpage
	sed -i -e 's:builtin.1::' -e '8s:MLINKS:_MLINKS:' "${S}/man/man1/Makefile"

	# Don't install the arch-specific directories in subdirectories
	sed -i -e '/MANSUBDIR/d' "${S}"/man/man4/man4.{i386,sparc64}/Makefile

	# Remove them so that they can't be included by error
	rm -rf "${S}"/mk/*.mk
}

src_compile() {
	export ESED="/usr/bin/sed"

	# This is a groff problem and not a -shared problem.
	export GROFF_TMAC_PATH="/usr/share/tmac/:/usr/share/groff/1.19.1/tmac/"
	mkmake || die "emake failed"
}

src_install() {
	mkmake -j1 DESTDIR="${D}" DOCDIR=/usr/share/doc/${PF} install || die "Install failed"
	rm -rf "${D}"/usr/share/locale
}
