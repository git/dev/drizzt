# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-freebsd/freebsd-sbin/freebsd-sbin-6.2-r1.ebuild,v 1.1 2007/04/06 14:51:22 uberlord Exp $

inherit flag-o-matic bsdmk freebsd

DESCRIPTION="FreeBSD sbin utils"
KEYWORDS=" ~x86-fbsd"
SLOT="0"

SRC_URI="http://84.33.1.46/~drizzt/stages/${SBIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${CONTRIB}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${LIB}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${LIBEXEC}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${USBIN}.tar.bz2
	http://84.33.1.46/~drizzt/stages/${ETC}.tar.bz2
	build? ( http://84.33.1.46/~drizzt/stages/${SYS}.tar.bz2 )"

RDEPEND="=sys-freebsd/freebsd-lib-${RV}*
	=sys-freebsd/freebsd-libexec-${RV}*
	ssl? ( dev-libs/openssl )
	dev-libs/libedit
	sys-libs/readline
	sys-process/vixie-cron"
DEPEND="${RDEPEND}
	!build? ( =sys-freebsd/freebsd-sources-${RV}* )
	=sys-freebsd/freebsd-mk-defs-${RV}*"

PROVIDE="virtual/dev-manager"

S="${WORKDIR}/sbin"

IUSE="atm ipfilter ipv6 vinum suid ssl build"

pkg_setup() {
	use atm || mymakeopts="${mymakeopts} NO_ATM= "
	use ipfilter || mymakeopts="${mymakeopts} NO_IPFILTER= "
	use ipv6 || mymakeopts="${mymakeopts} NO_INET6= "
	use vinum || mymakeopts="${mymakeopts} NO_VINUM= "
	use suid || mymakeopts="${mymakeopts} NO_SUID= "

	# O3 breaks this, apparently
	replace-flags -O3 -O2
}

REMOVE_SUBDIRS="dhclient pfctl pflogd rcorder"

PATCHES="${FILESDIR}/${PN}-setXid.patch
	${FILESDIR}/${PN}-zlib.patch
	${FILESDIR}/${PN}-6.1-pr102701.patch"

src_unpack() {
	freebsd_src_unpack
	use build || ln -s "/usr/src/sys-${RV}" "${WORKDIR}/sys"
}

src_install() {
	freebsd_src_install
	keepdir /var/log

	# Allow users to use ping and other commands
	dodir /bin
	mv "${D}/sbin/ping" "${D}/bin/" || die "mv failed"

	newinitd "${FILESDIR}/devd.initd" devd
	newinitd "${FILESDIR}/ipfw.initd" ipfw
	newconfd "${FILESDIR}/ipfw.confd" ipfw
	newinitd "${FILESDIR}/sysctl.initd" sysctl

	# Gentoo devd.conf
	# devd_queue is a filter so that only the last event is applied to an
	# init script
	insinto /etc
	newins "${FILESDIR}/devd.conf" devd.conf
	exeinto /etc
	newexe "${FILESDIR}/devd_queue" devd_queue

	# Do we need pccard.conf if we have devd?
	# Maybe ship our own sysctl.conf so things like radvd work out of the box.
	cd "${WORKDIR}/etc/"
	insinto /etc
	doins defaults/pccard.conf minfree sysctl.conf

	# Install a crontab for adjkerntz
	insinto /etc/cron.d
	newins "${FILESDIR}/adjkerntz-crontab" adjkerntz

	# Install the periodic stuff (needs probably to be ported in a more
	# gentooish way)
	cd "${WORKDIR}/etc/periodic"

	doperiodic security \
		security/*.ipfwlimit \
		security/*.ip6fwlimit \
		security/*.ip6fwdenied \
		security/*.ipfwdenied

	use ipfilter && doperiodic security \
		security/*.ipf6denied \
		security/*.ipfdenied
}
