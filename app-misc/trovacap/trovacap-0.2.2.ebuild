# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs

DESCRIPTION="Utility to find italian CAP"
HOMEPAGE="http://www.digitazero.org/?p=41"
SRC_URI="http://www.digitazero.org/download/${P}_src.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="x11-libs/fltk
	=dev-db/sqlite-3*"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

S="${WORKDIR}"/${PN}

src_unpack() {
	unpack ${A}
	cd ${S}

	# Fix Makefile
	sed -i -e '/^CPP = /d' \
		-e 's/^LIBS = .*$/LIBS = `fltk-config --ldflags` `pkg-config sqlite3 xpm --libs`/' \
		-e 's/^CXXFLAGS = .*$/CXXFLAGS += `pkg-config sqlite3 xpm --cflags` `fltk-config --cflags`/' \
		-e 's/$(CPP)/$(CXX)/' Makefile || die

	# Change relative paths to an absolutes
	sed -i -e 's:./trovacap.db:/usr/share/trovacap/trovacap.db:' src/trovacap.cxx || die
	sed -i -e "s:./docs/trovacap.html:/usr/share/doc/${PF}/html/trovacap.html:" src/dlg_help.cxx || die
}

src_compile() {
	tc-export CXX
	emake || die
}

src_install() {
	dobin ${PN}
	insinto /usr/share/${PN}
	doins ${PN}.db
	dohtml docs/*
	dodoc README ChangeLog
}
