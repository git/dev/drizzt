# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-sound/amarok/amarok-1.4.5-r1.ebuild,v 1.7 2007/02/24 10:20:51 corsair Exp $

LANGS="af ar az bg br ca cs cy da de el en_GB es et fa fi fr ga gl he
hi hu is it ja ka km ko lt ms nb nl nn pa pl pt pt_BR ro ru rw se sk
sl sq sr sr@Latn sv ta tg th tr uk uz zh_CN zh_TW"
LANGS_DOC="da de es et fr it nl pl pt pt_BR ru sv"

USE_KEG_PACKAGING=1

inherit kde eutils flag-o-matic

PKG_SUFFIX=""

MY_P="${P/_/-}"
S="${WORKDIR}/${P/_/-}"

DESCRIPTION="Advanced audio player based on KDE framework."
HOMEPAGE="http://amarok.kde.org/"

SRC_URI="mirror://kde/stable/amarok/${PV}/src/${MY_P}.tar.bz2"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="aac kde mysql noamazon opengl postgres
visualization ipod ifp real njb mtp musicbrainz daap xine gstreamer"
# kde: enables compilation of the konqueror sidebar plugin

RDEPEND="kde? ( || ( kde-base/konqueror kde-base/kdebase ) )
	xine? ( >=media-libs/xine-lib-1.1.2_pre20060328-r8 )
	>=media-libs/taglib-1.4
	mysql? ( >=virtual/mysql-4.0 )
	postgres? ( dev-db/libpq )
	opengl? ( virtual/opengl )
	visualization? ( media-libs/libsdl
					 =media-plugins/libvisual-plugins-0.4* )
	ipod? ( >=media-libs/libgpod-0.4.2 )
	aac? ( media-libs/libmp4v2 )
	ifp? ( media-libs/libifp )
	real? ( media-video/realplayer )
	njb? ( >=media-libs/libnjb-2.2.4 )
	mtp? ( >=media-libs/libmtp-0.1.1 )
	musicbrainz? ( media-libs/tunepimp )
	=dev-lang/ruby-1.8*"

DEPEND="${RDEPEND}"

RDEPEND="${RDEPEND}
	app-arch/unzip
	gstreamer? ( media-sound/yauap )
	daap? ( www-servers/mongrel )"

PATCHES="${FILESDIR}/${P}-magnatune.patch ${FILESDIR}/${P}-xinesux.patch"

need-kde 3.3

pkg_setup() {
	if ! use xine && ! use gstreamer; then
		einfo
		elog "As you did not pick xine or gstreamer use flag, building"
		elog "xine only."
		einfo
	fi

	kde_pkg_setup
}

src_compile() {
	# Extra, unsupported engines are forcefully disabled.
	local myconf="$(use_enable mysql) $(use_enable postgres postgresql)
				  $(use_with opengl) --without-xmms
				  $(use_with visualization libvisual)
				  $(use_enable !noamazon amazon)
				  $(use_with ipod libgpod)
				  $(use_with aac mp4v2)
				  $(use_with ifp)
				  $(use_with real helix)
				  $(use_with njb libnjb)
				  $(use_with mtp libmtp)
				  $(use_with musicbrainz)
				  $(use_with daap)
				  --without-mas
				  --without-nmm"

	if ! use xine && ! use gstreamer; then
		myconf="${myconf} --with-xine"
	else
		myconf="${myconf} $(use_with xine) $(use_with gstreamer yauap)"
	fi

	kde_src_compile
}

src_install() {
	kde_src_install

	# As much as I respect Ian, I'd rather leave Amarok to use mongrel
	# from Portage, for security and policy reasons.
	rm -rf "${D}"/usr/share/apps/amarok/ruby_lib/rbconfig \
		"${D}"/usr/share/apps/amarok/ruby_lib/mongrel* \
		"${D}"/usr/share/apps/amarok/ruby_lib/rubygems* \
		"${D}"/usr/share/apps/amarok/ruby_lib/gem* \
		"${D}"/usr/$(get_libdir)/ruby_lib
}
