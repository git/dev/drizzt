# Copyright 2006 Timothy Redaelli
# Distributed under the terms of the GNU General Public License, v2 or later
# $Header: $

alias zgrep="grep --decompress"
alias bzgrep="grep --bz2decompress"
alias zegrep="egrep --decompress"
alias zfgrep="fgrep --decompress"
alias bzegrep="egrep --bz2decompress"
alias bzfgrep="fgrep --bz2decompress"
