#!/bin/bash
# Copyright 2007 Timothy Redaelli <drizzt@gentoo.org>
# Distributed under the terms of the GNU General Public License v2

function einfo() {
	echo -e " \e[32;01m*\e[0m $*"
}

function ewarn() {
	echo -e " \e[33;01m*\e[0m $*" >&2
}

CHOST=$(gcc -v 2>&1 | awk '/^Target: /{print $2}')
export AS="/usr/${CHOST}/bin/as"
export LD="/usr/${CHOST}/bin/ld"

if type ccache &>/dev/null; then
	einfo "ccache detected"
	export PATH="/usr/lib/ccache/bin/:$PATH"
	export CC="/usr/lib/ccache/bin/${CHOST}-gcc"
	export CXX="/usr/lib/ccache/bin/${CHOST}-g++"
fi

if type distcc &>/dev/null; then
	einfo "distcc detected"
	if [[ -z "$DISTCC_HOSTS" ]]; then
		for i in $(</etc/distcc/hosts); do
			ip=${i%/*}
			port=${ip#*:}
			[[ "${port}" = "${ip}" ]] && port=3632 || ip=${ip%:*}
			if ( > "/dev/tcp/${ip}/${port}" ) 2>/dev/null || [[ ${ip} = localhost ]]; then
				DISTCC_HOSTS="${DISTCC_HOSTS} ${i}"
			else
				ewarn "${ip}:${port} is not available."
			fi
		done
	fi
	if [[ "${DISTCC_HOSTS}" ]]; then
		export DISTCC_HOSTS=${DISTCC_HOSTS}
		MAKEOPTS="-j$(($(for i in ${DISTCC_HOSTS}; do echo -n ${i#*/}+; done ; echo 0)))"
		if type ccache &>/dev/null; then
			export CCACHE_PREFIX="distcc"
		else
			export PATH="/usr/lib/distcc/bin/:$PATH"
			export CC="/usr/lib/distcc/bin/${CHOST}-gcc"
			export CXX="/usr/lib/distcc/bin/${CHOST}-g++"
		fi
		einfo "I'm using distcc with \"${DISTCC_HOSTS}\""
	 fi
fi

make -e ${MAKEOPTS} "$@"
