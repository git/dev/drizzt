# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Random scripts that drizzt finds useful"
HOMEPAGE="http://drizzt.bsdnet.eu/"
SRC_URI="http://dev.gentoo.org/~robbat2/earch-0.9.2"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="ruby"

RDEPEND="ruby? ( dev-ruby/sqlite3-ruby )
	app-portage/gentoolkit-dev"
DEPEND=""

src_install() {
	newbin "${DISTDIR}"/earch-0.9.2 earch
	use ruby && dobin "${FILESDIR}"/rdep
	dobin "${FILESDIR}"/ecommit "${FILESDIR}"/fix_overlay "${FILESDIR}"/dmake \
	"${FILESDIR}"/gen_metadata
}
