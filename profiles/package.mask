# Timothy Redaelli <drizzt@gentoo.org> (07 Jan 2007)
# Scm versions
=net-irc/weechat-9999
=sys-apps/paludis-9999
=dev-util/codeblocks-9999
=games-arcade/nogravity-9999
=media-video/ffmpeg-9999

# Timothy Redaelli <drizzt@gentoo.org> (15 Dec 2006)
# Beta version
=net-p2p/btg-0.8_pre*
