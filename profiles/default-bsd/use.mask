# Copyright 2004 Gentoo Foundation.
# Distributed under the terms of the GNU General Public License, v2
# $Header: /var/cvsroot/gentoo-x86/profiles/default-bsd/use.mask,v 1.31 2007/03/22 05:04:43 beandog Exp $

# Linux-dependant flags
selinux
nptl
alsa
aoss
v4l
v4l2
fbcon
directfb
ev6
hal
gpm
lirc
multilib
sysfs
lm_sensors
nodroproot
caps
vidix
madwifi
jack
3dfx
pmount
dxr3
nvidia
win32codecs
xvmc
xfs

# Steve Dibb <beandog@gentoo.org> (21 Mar 2007)
# media-video/mplayer local use flags
ivtv
live
dv
ggi
x264
xanim

# This usually refers just to linux-dvb
dvb

# Hardened doesn't work on Gentoo/*BSD for now
hardened

# termcap-compat doesn't work on BSD
termcap-compat

# useflags for php commercial software
hyperwave-api
interbase
mnogosearch
msql
ovrimos
sapdb

# Java requires that new-style virtuals are prepared for kaffe
java
gcj

# This requires a kernel module that's Linux specific right now
ifp

# This requires hal and a usb-eject command
ipod

# Mask extraneous elibc/kernel combinations
-userland_BSD
elibc_glibc
kernel_linux
userland_GNU

# Mask Xorg-modular drivers that are Linux-specific
input_devices_aiptek
input_devices_evdev
input_devices_linuxwacom
input_devices_synaptics
input_devices_ur98
input_devices_vmmouse
video_cards_fglrx
video_cards_newport
video_cards_sisusb
video_cards_v4l
video_cards_vmware
video_cards_nvidia

# Apache's MPM-itk requires sys-libs/libcap
mpm-itk
