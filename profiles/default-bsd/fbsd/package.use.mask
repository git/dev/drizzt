app-text/crm114 mew mimencode test

# ptex isn't tested yet
app-text/xdvik cjk

# Threaded Postgres is a total no go - initdb hands and psql
# segfaults when result set requires a pager
dev-db/libpq threads
dev-db/postgresql threads

# Threaded Ruby (1.8) on FreeBSD segfaults
dev-lang/ruby threads

# Somehow, check's tests are broken..
dev-libs/confuse test
dev-util/checkstyle test

# Dillo won't run on FreeBSD
# PDA masked until we can do some testing on it
mail-client/claws-mail dillo pda

# Python support isn't tested (Twisted for instance)
net-dns/avahi python

# Original spell plugin uses ispell that requires
# miscfiles.. that in turn does not work
net-irc/rbot spell

# Diego Pettenò <flameeyes@gentoo.org> (18 Jan 2007)
# Vixie-Cron uses pam useflag for pam_limits.so that is provided
# by Linux-PAM we don't use for now.
sys-process/vixie-cron pam

x11-misc/xscreensaver new-login

# media-libs/mesa with xcb actually doesn't work
media-libs/mesa xcb

# net-wireless/aircrack-ng - wireless capture tools not needed.
net-wireless/aircrack-ng wifi

# sane-backends not yet keyworded, xv doesn't build
dev-python/imaging scanner X
