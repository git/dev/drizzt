# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit distutils

MY_PN=Mixminion
MY_PV=${PV/_}
MY_P=${MY_PN}-${MY_PV}

DESCRIPTION="A Type III Anonymous Remailer"
HOMEPAGE="http://mixminion.net/"
SRC_URI="http://mixminion.net/dist/${MY_PV}/${MY_P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-libs/openssl-0.9.7"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${MY_P}

src_unpack() {
	unpack ${A}
	epatch "${FILESDIR}"/${P}-test.patch
}
