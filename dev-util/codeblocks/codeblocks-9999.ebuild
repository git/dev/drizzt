# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_AUTOMAKE="1.7"
WANT_AUTOCONF="2.5"

inherit autotools wxwidgets subversion flag-o-matic eutils autotools
#ESVN_CO_DIR="${PORTAGE_ACTUAL_DISTDIR-${DISTDIR}}"/svn-src/${P/-svn}/"${ESVN_REPO_URI##*/}"

ESVN_REPO_URI="svn://svn.berlios.de/codeblocks/trunk"
ESVN_PROJECT="${P}"

WX_GTK_VER="2.6"

DESCRIPTION="free cross-platform C/C++ IDE"
HOMEPAGE="http://www.codeblocks.org/"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"

IUSE="contrib unicode xrc monolithic static vanilla debug"

RDEPEND="=x11-libs/wxGTK-2.6*"

DEPEND="${RDEPEND}
	app-arch/zip"

pkg_setup() {
	if use unicode; then
		need-wxwidgets "unicode"
	else
		need-wxwidgets "gtk2"
	fi
}

src_unpack() {
	local wc_path=${ESVN_STORE_DIR}/${ESVN_PROJECT}/${ESVN_REPO_URI##*/}
	subversion_src_unpack

	use vanilla || \
	EPATCH_SUFFIX=patch epatch "${FILESDIR}"/patches

	# Setting revision
	echo "m4_define([SVN_REVISION], trunk-r${ESVN_WC_REVISION})" > ./revision.m4
	sed -i -e "s:\$(top_srcdir):\"${wc_path}\":" \
		src/build_tools/autorevision/Makefile.am || die

	eautoreconf
}

src_compile() {
	filter-flags -fomit-frame-pointer
	append-flags -fno-strict-aliasing

	econf --with-wx-config=wx-config-2.6 --enable-dependency-tracking \
	$(use_enable contrib) $(use_enable unicode) $(use_enable xrc) \
	$(use_enable monolithic) $(use_enable debug) \
	$(use_enable static) || die

	emake clean-zipfiles || die
	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die

	# Projectwizard needs a all-in-one-directory for wx envvar
	# (finding wxWidgets files/headers). Must be a dir with include
	# and lib under it, so we create symlinks:
	dodir /usr/share/wx

	rm -f "${D}"/usr/share/wx/{include,lib}
	ebegin "Creating wxGTK symlinks in /usr/share/wx"
	dosym /usr/include/wx-2.6 /usr/share/wx/include &&
	dosym /usr/lib/wx/include/gtk2-$(use unicode && echo -n unicode || echo -n ansi)-release-2.6 /usr/share/wx/lib &&
	eend || die
}
