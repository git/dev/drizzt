# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-util/kdesvn/kdesvn-1.0.4.ebuild,v 1.3 2008/12/21 10:35:18 george Exp $

EAPI=1

inherit cmake-utils eutils kde-functions versionator
set-kdedir 3.5
need-kde 3.3

My_PV=$(get_version_component_range 1-2)

DESCRIPTION="KDESvn is a frontend to the subversion vcs."
HOMEPAGE="http://www.alwins-world.de/wiki/programs/kdesvn"
SRC_URI="http://kdesvn.alwins-world.de/trac.fcgi/downloads/${P}.tar.bz2"

SLOT="3.5"
LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

RDEPEND=">=dev-util/subversion-1.4
	kde-base/kdesdk-kioslaves:3.5
	dev-db/sqlite"

DEPEND="${RDEPEND}
	>=dev-util/cmake-2.4"

EXTRA_ECONF+="-DCMAKE_INSTALL_PREFIX=${KDEDIR}"

LANGS="ca cs de es fr gl it ja lt nl pa ru sv"

for X in ${LANGS} ; do
	IUSE="${IUSE} linguas_${X}"
done

src_unpack() {
	unpack ${A}

	cd "${S}"

	# Apply fedora patch for as-needed (modified to not force as-needed)
	epatch "${FILESDIR}"/${PN}-1.0.0-asneeded.patch

	for X in ${LANGS} ; do
		use linguas_${X} || rm -f po/"${X}."*
	done

	# this seems to be again necessary
	sed -i -e "s:\${APR_CPP_FLAGS}:\${APR_CPP_FLAGS} \"-DQT_THREAD_SUPPORT\":" \
		"${S}"/CMakeLists.txt || die "QT_THREAD_SUPPORT sed failed"

	# Don't compile kio_svn (we already have it from kdesdk-kioslaves)
	sed -i '/kiosvn/d' "${S}"/src/CMakeLists.txt
}

pkg_postinst() {
	if ! has_version 'kde-base/kompare'; then
		echo
		elog "For nice graphical diffs, install kde-base/kompare."
		echo
	fi
}
