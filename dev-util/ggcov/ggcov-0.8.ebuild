# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A GTK+ GUI for exploring test coverage data produced by C and C++ programs compiled with gcc -fprofile-arcs -ftest-coverage."
HOMEPAGE="http://ggcov.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="gnome"

RDEPEND="dev-libs/popt
	>=dev-libs/glib-2
	sys-devel/binutils
	gnome? ( gnome-base/libgnomeui )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig"

RESTRICT=test

src_test() {
	CCACHE_DISABLE=1 emake check || die
}

src_compile() {
	econf `use_enable gnome gui` --disable-web
	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die
}
