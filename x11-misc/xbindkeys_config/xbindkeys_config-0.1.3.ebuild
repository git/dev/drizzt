# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs

DESCRIPTION="An easy to use gtk program for configuring Xbindkeys"
HOMEPAGE="none"
SRC_URI="ftp://ftp.FreeBSD.org/pub/FreeBSD/ports/local-distfiles/trevor/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="=x11-libs/gtk+-1.2*"
RDEPEND="${DEPEND}
	x11-misc/xbindkeys"

src_unpack() {
	unpack ${A}
	cd "${S}"
	sed -i -e '/^CC=/d' Makefile || die
}

src_compile() {
	emake CC="$(tc-getCC) ${CFLAGS}" || die
}

src_install() {
	dobin ${PN}
	dodoc AUTHORS BUGS ChangeLog README TODO 
}
