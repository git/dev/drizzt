# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

ECVS_SERVER="${PN}.cvs.sourceforge.net:/cvsroot/${PN}"
ECVS_MODULE="${PN}"

inherit games cvs

DESCRIPTION="a 3D arcade space shooter"
HOMEPAGE="http://www.realtech-vr.com/nogravity/"
SRC_URI="mirror://sourceforge/${PN}/rt-${PN}-src.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="debug openal opengl"

RDEPEND="opengl? ( virtual/opengl )
	media-libs/libsdl
	media-libs/libvorbis
	openal? ( media-libs/openal )
	!openal? ( media-libs/sdl-mixer )"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
	>=sys-devel/autoconf-2.50
	>=sys-devel/automake-1.9"

S="${WORKDIR}/${PN}"

pkg_setup() {
	if use opengl && ! built_with_use media-libs/libsdl opengl; then
		die "You need to compile media-libs/libsdl with USE=opengl."
	fi
}

src_unpack() {
	cvs_src_unpack
	cd "${S}"
	epatch "${FILESDIR}"/${PN}-as-needed.patch
}

src_compile(){
	local myconf=""
	cd "${S}/src/Linux"
	./bootstrap
	use openal && myconf="--enable-sound=openal" || myconf="--enable-sound=sdl_mixer"
	econf \
		$(use_enable debug) \
		$(use_enable opengl) \
		${myconf} || die
	emake || die
}

src_install() {
	dogamesbin src/Linux/nogravity || die "install failed"
	dodoc README.TXT

	prepgamesdirs
}
