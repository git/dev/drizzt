# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit games

DESCRIPTION="Chess for the Truly Hardcore"
HOMEPAGE="http://brutalchess.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${PN}-alpha-${PV}-src.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="media-libs/freetype
	media-libs/libsdl
	media-libs/sdl-image
	x11-libs/libX11"
RDEPEND="${DEPEND}"

src_install() {
	emake install DESTDIR="${D}"
}
