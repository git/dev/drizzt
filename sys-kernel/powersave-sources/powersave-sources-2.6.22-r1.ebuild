# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:$

ETYPE="sources"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"

inherit kernel-2
detect_version
detect_arch

DESCRIPTION="Software Suspend 2 + HRTimers"
HOMEPAGE="http://forums.gentoo.org/viewtopic-t-564588.html"

SUSPEND2_VERSION="2.2.10.1"
SUSPEND2_TARGET="2.6.22-rc4"
SUSPEND2_SRC="suspend2-${SUSPEND2_VERSION}-for-${SUSPEND2_TARGET}"
SUSPEND2_URI="http://www.suspend2.net/downloads/all/${SUSPEND2_SRC}.patch.bz2"

HRTIMERS_VERSION=${PR:1}
HRTIMERS_TARGET=${PV/_rc/-rc}
HRTIMERS_SRC="patch-${HRTIMERS_TARGET}-hrt${HRTIMERS_VERSION}"
HRTIMERS_URI="http://www.tglx.de/projects/hrtimers/${HRTIMERS_TARGET}/${HRTIMERS_SRC}.patch"

RADEON_SRC="radeon_vblanks"
RADEON_URI="http://www.minimalblue.com/ftp/public/linux/powersave-patches/${RADEON_SRC}.patch"

BGTIMERS_SRC="kernel-2.6.22-rc1"
BGTIMERS_URI="http://www.linuxpowertop.org/patches/${BGTIMERS_SRC}.patch"

UNIPATCH_LIST="${DISTDIR}/${HRTIMERS_SRC}.patch ${DISTDIR}/${RADEON_SRC}.patch
	${T}/${BGTIMERS_SRC}.patch ${DISTDIR}/${SUSPEND2_SRC}.patch.bz2"
UNIPATCH_STRICTORDER="yes"
SRC_URI="${KERNEL_URI} ${SUSPEND2_URI} ${HRTIMERS_URI} ${RADEON_URI}
	${BGTIMERS_URI}"

KEYWORDS="~amd64 ~x86"

RDEPEND="${RDEPEND}
		>=sys-apps/suspend2-userui-0.7.1
		>=sys-power/hibernate-script-1.95"

pkg_postinst() {
	kernel-2_pkg_postinst
	einfo "For more info on this patchset, and how to report problems, see:"
	einfo "${HOMEPAGE}"
}

pkg_setup() {
	ebegin "Apply a dos2unix to ${BGTIMERS_SRC}.patch"
	sed 's/\r$//' "${DISTDIR}"/${BGTIMERS_SRC}.patch > "${T}"/${BGTIMERS_SRC}.patch
	eend $?
}
