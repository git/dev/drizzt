# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit kde

DESCRIPTION="A ctags plugin for the KDE editor kate."
HOMEPAGE="http://www.kde-apps.org/content/show.php?content=47743"
SRC_URI="http://www.kde-apps.org/content/files/47743-${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="|| ( kde-base/kate kde-base/kdebase )"
RDEPEND="${DEPEND}
	dev-util/ctags"

need-kde 3.5
