# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit versionator kde

MY_PN=${PN/kate-/}
MY_PV=$(replace_version_separator 3 '-')
MY_P=${MY_PN}-${MY_PV}
DESCRIPTION="A projectmanager plugin for the KDE editor kate."
HOMEPAGE="http://www.kde-apps.org/content/show.php?content=42653"
SRC_URI="mirror://sourceforge/kate-prj-mng/${MY_P}.tar.bz2"
S="${WORKDIR}/${MY_PN}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="kde-base/kate"
RDEPEND="${DEPEND}"

need-kde 3.5
