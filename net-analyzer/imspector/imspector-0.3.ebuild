# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs

DESCRIPTION="Instant Messenger transparent proxy with logging capabilities."
HOMEPAGE="http://www.imspector.org/"
SRC_URI="http://www.imspector.org/downloads/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_unpack() {
	unpack ${A}

	sed -i -e 's/^CXXFLAGS =.*/CXXFLAGS += -fPIC/' "${S}"/Makefile
	sed -i -e '/#define DEFAULT_CONFIG/s:/usr::' "${S}"/main.cpp
}

src_compile() {
	emake CXX="$(tc-getCXX)" || die 'emake failed'
}

src_install() {
	dosbin imspector || die 'install failed'
	dolib.so libimspector.so
	insinto /usr/lib/imspector
	insopts -m0755
	doins *plugin.so
	insinto /etc/imspector
	insopts -m0644
	doins imspector.conf badwords.txt
}
