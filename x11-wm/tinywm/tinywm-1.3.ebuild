# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit toolchain-funcs

DESCRIPTION="A tiny window manager created as an exercise in minimalism."
HOMEPAGE="http://incise.org/index.cgi/TinyWM"
SRC_URI="http://incise.org/files/dev/${P}.tgz"

LICENSE="as-is"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND="x11-libs/libX11"
RDEPEND="${DEPEND}"

src_compile() {
	$(tc-getCC) -o ${PN} ${CFLAGS} ${LDFLAGS} ${PN}.c -lX11 || die
}

src_install() {
	dobin ${PN}
	dodoc README tinywm.py
}
