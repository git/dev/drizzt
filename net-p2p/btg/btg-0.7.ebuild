# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_AUTOMAKE=1.9

inherit toolchain-funcs autotools

DESCRIPTION="Bittorrent client implemented in C++."
HOMEPAGE="http://btg.berlios.de/"
SRC_URI="http://download.berlios.de/btg/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc ncurses gtk cppunit session-saving"

DEPEND="~net-libs/libtorrent_rb-0.10
	!>=net-libs/libtorrent_rb-0.11
	>=dev-libs/libsigc++-2
	doc? ( app-doc/doxygen )
	ncurses? ( >=sys-libs/ncurses-5 )
	gtk? ( >=dev-cpp/gtkmm-2.4 )
	cppunit? ( dev-util/cppunit )"

RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}"/${P}-as-needed.patch
	eautomake
}

src_compile() {
	./configure \
		$(use_enable debug) \
		$(use_enable ncurses cli) \
		$(use_enable gtk gui) \
		$(use_enable cppunit) \
		$(use_enable session-saving) \
		--with-boost-iostreams=boost_iostreams-mt \
		--with-boost-filesystem=boost_filesystem-mt \
		--with-boost-thread=boost_thread-mt \
		--with-boost-date-time=boost_date_time-mt \
		--with-boost-program_options=boost_program_options-mt

	emake CXX="$(tc-getCXX)" || die
}

src_install() {
	use doc && emake doxygen
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS NEWS README
}
